import Vue from "vue";
import Vuex from "vuex";
import moduleList from "@/api/modules-list.json";
import userList from "@/api/users-db.json";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    modules: moduleList
  },
  getters: {
    userInfo: id => {
      return userList.find(user => user.id === id)[0];
    }
  },
  mutations: {},
  actions: {},
  modules: {}
});
